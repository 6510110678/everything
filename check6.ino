#include <Keypad_I2C.h>
#include <Keypad.h>
#include <Wire.h>
#include <LiquidCrystal.h>
#include <Wire.h>
#include "RTClib.h"
#include <Tone32.h>

#define I2CADDR 0x20
#define BUZZER_PIN 12
#define BUZZER_CHANNEL 0

RTC_DS1307 rtc;
char daysOfTheWeek[7][12] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
DateTime now;

LiquidCrystal lcd1( 2, 15, 17, 16, 4, 0 );

const byte ROWS = 4;
const byte COLS = 4;

char hexaKeys[ROWS][COLS] = {
  {'1','2','3','A'},
  {'4','5','6','B'},
  {'7','8','9','C'},
  {'*','0','#','D'}
};

byte rowPins[ROWS] = {7, 6, 5, 4};
byte colPins[COLS] = {3, 2, 1, 0};
Keypad_I2C keypad( makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS, I2CADDR);

int alarmHour = 0;
int alarmMinute = 0;
bool alarmEnabled = false;
bool alarmSounded = false;

void I2C_bus_scan(void);

void setup(){
  lcd1.begin(16, 2); // จอกว้าง 16 ตัวอักษร 2 บรรทัด
  lcd1.clear(); // ล้างหน้าจอ
  Wire.begin( );
  keypad.begin( );
  I2C_bus_scan();
  if (! rtc.begin()) {
    lcd1.println("Couldn't find RTC");
    while (1);
  }
  rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
}

void loop() {
  display_date_lcd();
  setAlarm();
  check_alarm();
}

void setAlarm(){
  char key = keypad.getKey();
  if (key != NO_KEY) {
    lcd1.clear();
    delay(1000);
    lcd1.clear();
    if (key == 'C') {
      alarmEnabled = false;
      lcd1.clear();
      lcd1.setCursor(0, 1);
      lcd1.print("Alarm Cancelled");
      delay(2000);
      lcd1.clear();
    }
    else if (key == 'A') {
      int hours = 0;
      int minutes = 0;
      lcd1.setCursor(0, 0);
      lcd1.print("Set Time");
      lcd1.clear();
      lcd1.setCursor(0, 0);
      lcd1.print("hours: ");
      lcd1.setCursor(7, 0);
      while (true) {
        char digit = keypad.getKey();
        if (digit == '#') {
          break;
        }
        
        if (digit >= '0' && digit <= '9') {
          hours = (hours * 10) + (digit - '0');
          lcd1.print(digit);
          if (hours > 24) {
            lcd1.clear();
            lcd1.setCursor(2,1);
            lcd1.print("No more >= 24");
            break;
          }
          else if (hours == 24) {
            hours = 0;
          }
        }
        delay(200);
      }
      lcd1.setCursor(0, 1);
      lcd1.print("Hours set: ");
      lcd1.setCursor(11, 1);
      lcd1.print(hours);
      delay(2000);
      lcd1.clear();

      lcd1.setCursor(0, 0);
      lcd1.print("minutes: ");
      lcd1.setCursor(9, 0);
      while (true) {
        char digit = keypad.getKey();
        if (digit == '#') {
          break;
        }
        
        if (digit >= '0' && digit <= '9') {
          minutes = (minutes * 10) + (digit - '0');
          lcd1.print(digit);
          if (minutes >= 60){
            lcd1.clear();
            lcd1.setCursor(2,0);
            lcd1.print("No more > 60");
            break;
          }
          if (minutes == 60){
            minutes = 0;
            hours ++;
            if (hours == 24) {
                hours = 0;
            }
          }
        }
        delay(200);
      }
      lcd1.setCursor(0, 1);
      lcd1.print("Minutes set: ");
      lcd1.setCursor(13, 1);
      lcd1.print(minutes);
      delay(2000);
      lcd1.clear();

      lcd1.setCursor(0,0);
      lcd1.print("Alarm Clock Set");
      lcd1.setCursor(6,1);
      lcd1.print(hours);
      lcd1.print(":");
      lcd1.print(minutes);

      alarmHour = hours;
      alarmMinute = minutes;
      alarmEnabled = true;

      delay(5000);
    }
    delay(2000);
  }
}


void I2C_bus_scan(void)
{
  Serial.println ();
  Serial.println ("www.9arduino.com ...");
  Serial.println ("I2C scanner. Scanning ...");
  byte count = 0;
  Wire.begin();
  for (byte i = 8; i < 120; i++) // Loop ค้นหา Address
  {
    Wire.beginTransmission (i);
    if (Wire.endTransmission () == 0)
    {
      Serial.print ("Found address: ");
      Serial.print (i, DEC);
      Serial.print (" (0x");
      Serial.print (i, HEX);
      Serial.println (")");
      count++;
      delay (1);
    }
  }
  Serial.println ("Done.");
  Serial.print ("Found ");
  Serial.print (count, DEC);
  Serial.println (" device(s).");
}

void display_date_lcd(void)
{
  now = rtc.now();
  lcd1.setCursor(0,0);
  lcd1.print(daysOfTheWeek[now.dayOfTheWeek()]);
  lcd1.print(" , ");
  lcd1.print(now.day(), DEC);
  lcd1.print("/");
  lcd1.print(now.month(), DEC);
  lcd1.print("/");
  lcd1.print(now.year(), DEC);
  lcd1.setCursor(0,1);
  lcd1.print("  Time:");
  lcd1.print(now.hour(), DEC);
  lcd1.print(":");
  lcd1.print(now.minute(), DEC);
  lcd1.print(":");
  lcd1.print(now.second(), DEC);
  if (now.second() == 59){
    lcd1.clear();
  }
}

void check_alarm() {
  if (alarmEnabled && !alarmSounded && now.hour() == alarmHour && now.minute() == alarmMinute) {
      lcd1.clear();
      lcd1.setCursor(2, 0);
      lcd1.print("Wake UP !!!");
      for (int i = 0; i < 10; i++) {
      tone(BUZZER_PIN, NOTE_G4, 1000, BUZZER_CHANNEL); 
      delay(500);
    }
    alarmEnabled = false;
    alarmSounded = true;
  }
  if (!alarmEnabled && alarmSounded) {
      noTone(BUZZER_PIN);

  }
}

